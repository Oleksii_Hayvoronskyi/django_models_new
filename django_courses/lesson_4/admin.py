from django.contrib import admin
from . import models

# Реєструю в адмінці свої моделі.
admin.site.register(models.Example)
admin.site.register(models.Book)
admin.site.register(models.Place)
admin.site.register(models.Restaurant)
admin.site.register(models.Waiter)
admin.site.register(models.Publication)
admin.site.register(models.Article)


# Клас для зміни видимої чатсини адмінки
class AuthorAdmin(admin.ModelAdmin):
    # list_display = ['name', 'surname'] # Відобразить два стовпчики в адмінці.
    # Відобразить всі назви полів, які є в моделі Author.
    list_display = [field.name for field in models.Author._meta.fields]
    # exclude = ['name']                    # Приховає поле name.
    # fields = ['name']                     # Відобразить тільки поле name.
    list_filter = ['name']                  # Фільтрування за іменем
    # search_fields = ['name', 'surname']   # Поле для пошуку.

    # Відобразить всі поля.
    search_fields = [field.name for field in models.Author._meta.fields]

    class Meta:
        model = models.Author


admin.site.register(models.Author, AuthorAdmin)

