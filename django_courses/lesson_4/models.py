from django.db import models


# Cтворення моделі з різними параметрами.
class Example(models.Model):
    integer_field           = models.IntegerField()
                            # Лише числа позитивні (без мінуса).
    positive_field          = models.PositiveIntegerField()
    boolean_field           = models.BooleanField()
                            # Звичайні рядкові дані (кількість символів - 5).
    char_field              = models.CharField(max_length=5)
    text_field              = models.TextField(max_length=20)
                            # Можна встановити тільки дату.
    date_field              = models.DateField(auto_now=False)
                            # Можна встановити дату та час.
    data_time_field         = models.DateTimeField(auto_now_add=False)
    decimal_field           = models.DecimalField(max_digits=5, decimal_places=2)
    email                   = models.EmailField()
    file_field              = models.FileField(upload_to='files')
    image_field             = models.ImageField(upload_to='images')


# ЗВ’ЯЗОК --> ОДИН ДО БАГАТЬОХ на прикладі авторів та їх книжок:
class Author(models.Model):
    _meta = None
    name            = models.CharField(max_length=50)
    surname         = models.CharField(max_length=50)
    date_birth      = models.DateField(auto_now=False)

    # Визначаю метод для відображення в адмініці імен авторів.
    def __str__(self):
        # Поверне ім’я та прізвище автора через пробіл
        return self.name + ' ' + self.surname


class Book(models.Model):
    # Додав кортех з жанрами книги: 1-ше значення - для адмінки, 2-ге - те, що побачить користувач.
    CHOICE_GENRE = (
        ('comedy', 'Comedy'),
        ('action', 'Action'),
        ('thriller', 'Thriller'),
    )
    author      = models.ForeignKey(Author, on_delete=models.CASCADE)
    title       = models.CharField(max_length=50)
    text        = models.TextField(max_length=1000)
    genre       = models.CharField(max_length=50, choices=CHOICE_GENRE)

    # Визначаю метод для відображення в адмініці назв книжок.
    def __str__(self):
       return self.title


# ЗВ’ЯЗОК --> ОДИН ДО ОДНОГО на прикладі роботи ресторану:
class Place(models.Model):
    name        = models.CharField(max_length=50)
    address     = models.CharField(max_length=80)

    def __str__(self):
        return '%s the place' % self.name


class Restaurant(models.Model):
    place               = models.OneToOneField(Place, on_delete=models.CASCADE)
    serves_sandwich     = models.BooleanField(default=False)
    serves_pizza        = models.BooleanField(default=False)

    def __str__(self):
        return '%s the restaurant' % self.place.name


class Waiter(models.Model):
    restaurant        = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    name              = models.CharField(max_length=50)

    def __str__(self):
        return '%s the waiter at %s' % (self.name, self.restaurant)


# ЗВ’ЯЗОК --> БАГАТО ДО БАГАТЬОХ на прикладі різних часописів та статей, які в них опублікоані:
class Publication(models.Model):
    title               = models.CharField(max_length=50)

    def __str__(self):
        return self.title

    # Додатковий клас.
    class Meta:
        ordering = ('title',) # це потрбіно для віджета в адмін-частині.


class Article(models.Model):
    headline            = models.CharField(max_length=100)
    publications        = models.ManyToManyField(Publication)

    def __str__(self):
        return self.headline

    # Додатковий клас.
    class Meta:
        ordering = ('headline',)






